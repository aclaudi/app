GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
BINARY_NAME=app

all: build
build:
	CGO_ENABLED=0 $(GOBUILD) -ldflags='-s -w' -o $(BINARY_NAME) -v
test:
	$(GOTEST) -v ./...
clean:
	$(GOCLEAN)
	rm -f $(BINARY_NAME)
run:
	$(GOBUILD) -ldflags='-s -w' -o $(BINARY_NAME) -v ./...
	./$(BINARY_NAME)	
