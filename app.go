/*
 * @rotiscianibot is a telegram bot for the ADI Politburo chat,
 * derived from bottarga, a rough, rude, shameless Telegram bot.
 * Also this bot has its share of defects, of course.
 * Copyright (C) 2018  Matteo Croce <matteo@openwrt.org>
 * Copyright (C) 2018  Andrea Claudi <email@andreaclaudi.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

const (
	DB_USER     = "user"
	DB_PASSWORD = "password"
	DB_NAME     = "db"
)

func parseArgs(db *sql.DB) map[string]string {
	// setup and parse flags
	param := flag.String("param", "", "Value stored in db")
	flag.Parse()

	// insert config values in db
	if len(*param) > 0 {
		_, err := db.Exec("INSERT INTO config VALUES('param', $1) ON CONFLICT (key) DO UPDATE SET value = EXCLUDED.value;", *param)
		if err != nil {
			log.Fatal(err)
		}
	}

	config := map[string]string{}

	if rows, err := db.Query("SELECT * FROM config"); err == nil {
		for rows.Next() {
			var k, v string
			rows.Scan(&k, &v)
			config[k] = v
		}
		rows.Close()
	} else {
		log.Fatal("Query error")
	}

	if _, ok := config["param"]; !ok {
		log.Fatal("Missing param")
	}

	return config
}

func setupDB() *sql.DB {
	// open the sqlite db
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", dbinfo)
	
	// db does not exist, bail out
	if err != nil {
		log.Panic(err)
	}

	// db may be void, create tables
	_, err = db.Exec(`CREATE TABLE config (key TEXT PRIMARY KEY, value TEXT);`)
	if err != nil {
		log.Println(err)
	}

	return db
}

func main() {
	db := setupDB()
	config := parseArgs(db)

	log.Println(config)

}
